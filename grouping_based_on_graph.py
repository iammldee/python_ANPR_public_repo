# -*- coding: utf-8 -*-
"""
Created on Thu Feb 16 20:17:25 2017

@author: ik-be
"""
import pytesseract
from PIL import Image
pytesseract.pytesseract.tesseract_cmd = 'C:/Program Files (x86)/Tesseract-OCR/tesseract'

import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.gridspec as gridspec

from scipy import ndimage as ndi
from skimage import io
from skimage import transform as tf
from skimage.filters import threshold_otsu
from skimage.measure import label
from skimage.morphology import closing, square
from skimage.measure import regionprops
from skimage.morphology import watershed
from skimage.feature import peak_local_max


def apply_otsu(img, square_size = 1):
    thresh = threshold_otsu(img)
    return closing(img < thresh, square(square_size))

def get_label_image_with_otsu(imgray):
    bw = apply_otsu(imgray, square_size = 3)
    label_image = label(bw)
    borders = np.logical_xor(bw, bw)
    label_image[borders] = -1
    return bw, label_image    


class Find_candidates(object):
    
    
    def __init__(self, img):
        self.img = img

    def get_center_point(self, coords):
        y0,y1,x0,x1 = coords[0],coords[1],coords[2],coords[3]
        return [int((y1+y0)/2.0), int((x1+x0)/2.0)]
            
    def build_connected_graph(self, matrix):
        print matrix
        G = nx.to_networkx_graph(matrix.astype(int), create_using=nx.DiGraph())
        return nx.DiGraph.to_undirected(G) 
        
    def get_rectangle_indices(self, graph, index):
        return [i for i in nx.node_connected_component(graph, index)]    
        
    def get_subgraph(self, matrix, index):
        self.graph = self.build_connected_graph(matrix)
        return self.get_rectangle_indices(self.graph, index)        
        
    def make_connected_graph_array(self, character_property_list):
        n = len(character_property_list)
        width_magnify = 3
        connected_matrix = np.zeros((n, n))
        counter_ar = np.zeros(n)
        for i in range(n):
            for j in range(n):
                if i == j:
                    continue
                target = character_property_list[i]
                original = character_property_list[j]
    
                target_height = target[3]            
                target_width = target[4] * width_magnify
                
                high_filter_pass = False
                width_filter_pass = False
    
                if target[1][1] + target_height >= original[1][1] >= target[1][0] - target_height:
                    high_filter_pass = True
                
                if target[1][3] + target_width >= original[1][2] >= target[1][3] - target_width:
                    width_filter_pass = True
                    
                if high_filter_pass and width_filter_pass:
                    connected_matrix[i, j] = 1
                    counter_ar[i] += 1
                #If a rectangle is inside another rectangle than remove it from the counter
                if target[1][0] > original[1][0] and target[1][1] < original[1][1] and target[1][2] > original[1][2] and target[1][3] < original[1][3]:
                    counter_ar[i] -= 1
        return connected_matrix, counter_ar       

    def build_rectangle_property_list(self, label_img):
        self.standing_rectangle_list = []
        self.lying_rectangle_list = []
        for region in regionprops(label_img):
            if region.area < 50: #Ideally this threshold would be dynamic
                continue    
            X0, Y0, X1, Y1 = region.bbox
            ratio = ((Y1-Y0) / (X1 -float(X0)))  
            rectangle_properties = (abs(4.74 - ratio),  
                                        [int(X0), int(X1), int(Y0), int(Y1)], 
                                         int((X1 - X0)*(Y1 - Y0)), int(X1-X0), int(Y1-Y0))
            if (X1-X0) <= (Y1 - Y0):
                self.lying_rectangle_list.append(rectangle_properties)
            else:
                self.standing_rectangle_list.append(rectangle_properties)

    def get_character_group_index_list(self, character_property_list):
        self.connected_matrix, counter_ar = self.make_connected_graph_array(character_property_list)
        #The rectangle with the most connections will be the starting point
        if len(counter_ar) > 0: 
            highest_count_index = np.argmax(counter_ar)
            self.standing_rectangle_list_index = self.get_subgraph(self.connected_matrix, highest_count_index)
        else:
            raise Exception('Nothing found, halting - retry with a cut-off of the original image so the labelled region might find the license plate better / different one')

    def fetch(self):
        self.enclosed_img, self.label_img = get_label_image_with_otsu(imgray)
        self.build_rectangle_property_list(self.label_img)        
        self.get_character_group_index_list(self.standing_rectangle_list)

        
        
class Get_characters(object):
    
    
    def __init__(self, candidateObj):
        self.candidateObj = candidateObj

    def build_center_XY_vectors(self, standing_rectangle_list, standing_rectangle_list_index):
        self.char_candidate_properties = [standing_rectangle_list[i] for i in standing_rectangle_list_index]
        self.x_mid_list = [self.candidateObj.get_center_point(properties[1])[1] for properties in self.char_candidate_properties]
        self.y_mid_list = [self.candidateObj.get_center_point(properties[1])[0] for properties in self.char_candidate_properties]
        self.X = np.array(sorted(self.x_mid_list))
        self.Y = np.array(sorted(self.y_mid_list))
        
    def build_interdistance_list(self, X, Y):
        return [((X[i+1] - X[i])**2 + (Y[i+1] - Y[i])**2)**0.5 for i in range(len(X) - 1)]
                        
    def get_bin_members(self, bin_size_tuple, inter_distance_list):
        bin_members = []
        for inter_distance in inter_distance_list:
            if bin_size_tuple[1] >= inter_distance >= bin_size_tuple[0]:
                bin_members.append(inter_distance)
        return bin_members
        
    def filter_on_bin_count(self, inter_distance_list):
        bin_count, bin_sizes = np.histogram(inter_distance_list, bins = 3)
        if abs(bin_count[0] - bin_count[2]) > 3:
            if bin_count[0] > bin_count[2]: #Note the list index in the argument of self.get_bin_members
                self.new_index_list = self.get_bin_members(bin_sizes[:2], inter_distance_list)
            else:
                self.new_index_list = self.get_bin_members(bin_sizes[2:], inter_distance_list)
        else:
            self.new_index_list = inter_distance_list
 
    def get_coordinate_index_list(self, old_interdistance_list, new_interdistance_list):
        self.new_index_list = range(len(old_interdistance_list) + 1)
        if len(old_interdistance_list) != len(new_interdistance_list):
            for old in old_interdistance_list:
                if old not in new_interdistance_list:
                    removed_value_index = old_interdistance_list.index(old)
                    self.new_index_list.remove(removed_value_index)                
 
    def get_new_character_properties(self, character_properties, interdistance_index, 
                                     x_mid_list, y_mid_list):
        X0_Y0_coord_list, X1_Y1_coord_list = [], []
        new_X, new_Y = [], []
        for i in interdistance_index:
            X0_Y0_coord_list.append([character_properties[i][1][0], character_properties[i][1][2]])
            X1_Y1_coord_list.append([character_properties[i][1][1], character_properties[i][1][3]])            
            new_X.append(x_mid_list[i])
            new_Y.append(y_mid_list[i])    
        self.new_X, self.new_Y= np.array(new_X), np.array(new_Y)
        self.X0Y0, self.X1Y1 = np.array(X0_Y0_coord_list), np.array(X1_Y1_coord_list)
    
    def fetch(self):
        self.build_center_XY_vectors(self.candidateObj.standing_rectangle_list, 
                                     self.candidateObj.standing_rectangle_list_index)
        self.inter_distance_list = self.build_interdistance_list(self.X, self.Y)
        self.filter_on_bin_count(self.inter_distance_list)
        self.get_coordinate_index_list(self.inter_distance_list, 
                                       self.new_index_list)
        
        self.get_new_character_properties(self.char_candidate_properties, 
                                          self.new_index_list, 
                                          self.x_mid_list, self.y_mid_list)
        
        self.new_inter_distance_list = self.build_interdistance_list(self.new_X, self.new_Y)

        
class Rotate_and_clean(object):
    
    
    def __init__(self, characterObj):
        self.characterObj = characterObj
        self.candidateObj = self.characterObj.candidateObj
        self.angle_rotation_treshold = 0.1
        self.image_padding = 1.5
 
    def get_license_plate_angle(self, X, Y):
        m, c = np.polyfit(X, Y, 1)
        return m, c
                       
    def cut_letters(self, img, X0_Y0, X1_Y1):
        letter_imgs = []
        for x0_y0, x1_y1 in zip(X0_Y0, X1_Y1):
            letter = apply_otsu(img[x0_y0[0]: x1_y1[0], x0_y0[1]:x1_y1[1]])
            letter_imgs.append(letter)
        return letter_imgs
    
    def apply_rotation(self, letter_imgs, ANGLE):
        rotated_letters = []
        for letter in letter_imgs:
            tform = tf.SimilarityTransform(scale=1.1, rotation=ANGLE ,
                                   translation=(ANGLE * (np.shape(letter)[0] / 2.0), 
                                                -ANGLE * (np.shape(letter)[1] / 2.0))) #Negative due the Y-axis inversion of images
            rotated_letters.append(tf.warp(letter, tform))
        return rotated_letters
    
    def apply_watershed(self, imgs):
        thresh_imgs, labels = [], []
        for img in imgs:
            footprint_kernel_size = np.shape(img)[1] / 5
            distance = ndi.distance_transform_edt(img)
            local_maxi = peak_local_max(distance, indices=False, 
                                        footprint=np.ones((footprint_kernel_size,footprint_kernel_size)),
                                        labels=img)
            markers = ndi.label(local_maxi)[0]
            labelled = watershed(-distance, markers, mask=img)
            labels.append(labelled)
            thresh_imgs.append((labelled > 0))
        return thresh_imgs, labels   
    
    def rotate_characters(self, letter_imgs, ANGLE):
        if abs(ANGLE) > self.angle_rotation_treshold:  #Image quality deteriorates significantly by rotating
            self.rotated_letters = self.apply_rotation(letter_imgs, ANGLE)
        else:
            self.rotated_letters = letter_imgs
        return self.rotated_letters

    def add_borders_and_concat_images(self, img_list):
        x_sizes = [np.shape(img)[1] for img in img_list]
        y_sizes = [np.shape(img)[0] for img in img_list]
        y_size = int(max(y_sizes) * self.image_padding)
        x_size = int(max(x_sizes) * self.image_padding)
        merged_image = np.zeros((y_size, x_size))
        merged_img_list = []
        for i, img in enumerate(img_list):
            y_offset, x_offset = (y_size - y_sizes[i]) / 2, (x_size - x_sizes[i]) / 2
            merged_image[y_offset:y_offset+y_sizes[i], x_offset:x_offset+x_sizes[i]] = img
            merged_image[:y_offset, :] = 0
            merged_image[y_offset+y_sizes[i]:, :] = 0
            merged_image[:, :x_offset] = 0
            merged_image[:, x_offset+x_sizes[i]:] = 0  
            #Without the above block some artifacts may arise for reasons unknown           
            merged_img_list.append(merged_image.copy())

        return np.concatenate(merged_img_list, axis = 1)   
            
    def get_characters_to_string(self, image):
        #The config file license plate is a file that I have manually added into the 
        #\Tesseract-OCR\tessdata\configs folder.
        #It's important that on windows the file is saved as a UNIX file (LF).
        #else it's not able to read the parameters
        #The contents of the config file are as follows:
            #tessedit_char_whitelist 0123456789BDFGHJKLMNPRSTVWXZ
            
        im = Image.fromarray(np.uint8(image*255))
        return pytesseract.image_to_string(im, config = "license_plate_whitelisted_characters")
        
    def fetch(self):
        self.ANGLE, self.c = self.get_license_plate_angle(self.characterObj.new_X, 
                                                          self.characterObj.new_Y)

        self.letter_imgs = self.cut_letters(self.candidateObj.img, 
                                            self.characterObj.X0Y0, 
                                            self.characterObj.X1Y1)
        
        self.rotated_letters = self.rotate_characters(self.letter_imgs, self.ANGLE)
        self.character_imgs, self.labels = self.apply_watershed(self.rotated_letters) #Labels is only used to plot the result of the watershed algo.
        self.merged_character_image = self.add_borders_and_concat_images(self.character_imgs)
        self.character_string = self.get_characters_to_string(self.merged_character_image)

  
        
class Plot_stuff(object):
    
    
    def __init__(self, cleanObj, path):
        self.rgbimg = io.imread(path, as_grey = False)
        self.grayimg = io.imread(path, as_grey = True)
        self.cleanObj = cleanObj
        self.characterObj = self.cleanObj.characterObj
        self.candidateObj = self.cleanObj.characterObj.candidateObj
        self.plot_funcs = [self.plot_original_image,
                           self.plot_original_labelled_image,
                           self.plot_drawn_image_and_graph,
                           self.plot_centerpoints_and_histogram,
                           self.plot_deskewed_cleaned_letters]

        
    def invert_x_axis(self, coord_list):
        new_coord_list = []
        for (X0, X1, Y0, Y1) in coord_list:
            new_coord_list.append([-X0, -X1, Y0, Y1])
        return new_coord_list
        
    def restructure_X0Y0_X1Y1_into_coordlist(self, X0Y0, X1Y1):
        coord_list = []
        for (X0, Y0), (X1, Y1) in zip(X0Y0, X1Y1):
            coord_list.append([X0, X1, Y0, Y1])
        return coord_list
        
    def get_coord_list(self, rectangle_properties):
        return [rectangle_propertie[1] for rectangle_propertie in rectangle_properties] 

    def get_widdend_coord_list(self, rectangle_coords):
        enlarged_coords = []
        for coords in rectangle_coords:
            X0, X1, Y0, Y1 = coords
            width = Y1 - Y0
            height = X1 - X0
            enlarged_coords.append([X0 - height, X1 + height, Y0 - width * 3, Y1 + width * 3])
        return enlarged_coords
            
            
    def get_initial_coord_list(self):
        rectangle_coords = self.candidateObj.lying_rectangle_list
        rectangle_coords.extend(self.candidateObj.standing_rectangle_list)
        coord_list = self.get_coord_list(rectangle_coords)       
        return coord_list
        
    def draw_rectangles_on_image(self, img_plot, coord_list):    
        for (X0, X1, Y0, Y1) in coord_list:
            rect = mpatches.Rectangle((Y0, X0), Y1 - Y0, X1 - X0, fill=False, edgecolor='red', linewidth=2)
            img_plot.add_patch(rect)
        return img_plot

    def draw_lines(self, ax, X, Y):
        for i in range(len(X) - 1):
            ax.plot([X[i], X[i+1]], [Y[i], Y[i+1]])
        return ax
        
    def plot_original_image(self):
        plt.figure(figsize = (16, 10))
        plt.imshow(self.rgbimg)
        plt.axis('off')
        plt.title('Figure 1. Input Image')
        plt.show()
        
    def plot_original_labelled_image(self):
        coord_list = self.get_initial_coord_list()
        fig, ax = plt.subplots(nrows = 2, ncols = 2, figsize=(16, 10))
                        
        ax[0][0].imshow(self.rgbimg)
        ax[0][0].set_title('Figure 2a. Original input image')

        ax[0][1].imshow(self.candidateObj.enclosed_img)
        ax[0][1].set_title('Figure 2b. Closed Otsu threshold Image')
        
        ax[1][0].imshow(self.rgbimg)
        ax[1][0].set_title('Figure 2d. Rectangles drawn around image regions')
        ax[1][0] = self.draw_rectangles_on_image(ax[1][0], coord_list)
        
        ax[1][1].imshow(self.candidateObj.label_img)
        ax[1][1].set_title('Figure 2c. Labelled image regions')
        ax[1][1] = self.draw_rectangles_on_image(ax[1][1], coord_list)

        for ax_ar in ax:
            for a in ax_ar:
                a.set_axis_off()
        
        plt.tight_layout()
        plt.show()
        
        
    def plot_drawn_image_and_graph(self):
        coord_list = self.get_coord_list(self.candidateObj.standing_rectangle_list)
        enlarged_coord_list = self.get_widdend_coord_list(coord_list)
        candidate_coord_list = self.get_coord_list(self.characterObj.char_candidate_properties)
        graph = self.candidateObj.graph
        
        fig, ax = plt.subplots(nrows = 2, ncols = 2, figsize=(16, 10))
        
        ax[0][0].imshow(self.rgbimg)
        ax[0][0] = self.draw_rectangles_on_image(ax[0][0], coord_list)
        ax[0][0].set_title('Figure 3a. Only standing rectangles (height > width) are possible candidates.')
        ax[0][0].set_axis_off()
        
        ax[1][0].imshow(self.rgbimg)
        ax[1][0] = self.draw_rectangles_on_image(ax[1][0], enlarged_coord_list)
        ax[1][0].set_title('Figure 3b. Widenend rectangles')
        ax[1][0].set_axis_off()
        
        ax[0][1].set_title('Figure 3c. Connected graphs of the overlapping rectangles')
        nx.draw(graph, ax = ax[0][1])
        
        ax[1][1].set_title('Figure 3d. The largest connected graph is selected first.')
        ax[1][1].imshow(self.rgbimg)
        ax[1][1] = self.draw_rectangles_on_image(ax[1][1], candidate_coord_list)
        ax[1][1].set_axis_off()
        
        plt.show()
        
    def plot_centerpoints_and_histogram(self):

        candidate_coord_list = self.get_coord_list(self.characterObj.char_candidate_properties)
        filtered_candidate_coord_list = self.restructure_X0Y0_X1Y1_into_coordlist(self.characterObj.X0Y0, 
                                                                                  self.characterObj.X1Y1)

        inverted_candidate_coord_list = self.invert_x_axis(candidate_coord_list)
        inverted_filtered_candidate_coord_list = self.invert_x_axis(filtered_candidate_coord_list)
    
        scatter_aspect, hist_aspect = 1, 6
        gs = gridspec.GridSpec(4, 6)
        fig, ax = plt.subplots(nrows = 2, ncols = 6, figsize=(16, 10))

        ax0 = plt.subplot(gs[:, :2])
        ax0.imshow(self.rgbimg)
        ax0 = self.draw_rectangles_on_image(ax0, candidate_coord_list)
        ax0.set_title('Figure 4a. Candidate character regions')
        ax0.set_axis_off()        
        
        ax1 = plt.subplot(gs[1, 2:4])
        ax1 = self.draw_rectangles_on_image(ax1, inverted_candidate_coord_list)
        ax1.scatter(self.characterObj.X, - self.characterObj.Y)
        ax1 = self.draw_lines(ax1, self.characterObj.X, - self.characterObj.Y)
        ax1.set_aspect(scatter_aspect)
        ax1.set_title('Figure 4b. Candidate centerpoints. Before filter')

        ax2 = plt.subplot(gs[2, 2:4])
        ax2.set_aspect(hist_aspect)
        ax2.hist(self.characterObj.inter_distance_list, bins = 3)  
        ax2.set_title('Figure 4c. Histogram of the distance between the centerpoints, forced into 3 bins.')
       
        ax3 = plt.subplot(gs[1, 4:])
        ax3 = self.draw_rectangles_on_image(ax3, inverted_filtered_candidate_coord_list)
        ax3.scatter(self.characterObj.new_X, - self.characterObj.new_Y)
        ax3 = self.draw_lines(ax3, self.characterObj.new_X, - self.characterObj.new_Y)
        ax3.set_aspect(scatter_aspect)
        ax3.set_title('Figure 4d. Candidate centerpoints. After filter.')

        ax4 = plt.subplot(gs[2, 4:], sharey = ax2)
        ax4.set_aspect(hist_aspect)
        ax4.hist(self.characterObj.new_inter_distance_list, bins = 3)  
        ax4.set_title('Figure 4e. Histogram of the distance between the centerpoints. After filter.')
        plt.show()
        
    def plot_deskewed_cleaned_letters(self):
        args = [self.cleanObj.letter_imgs, 
                self.cleanObj.rotated_letters, 
                self.cleanObj.labels, 
                self.cleanObj.character_imgs]
                
        gs = gridspec.GridSpec(5, 6)
        fig, ax = plt.subplots(nrows = 5, ncols = 6, figsize=(16, 10))
        titles = ['Figure 5a. Original extracted characters',
                  'Figure 5b. Rotated image with visible artifacts',
                  'Figure 5c. Applied watershed algorithm', 
                  'Figure 5d. Characters ready for OCR']
        plt.axis('off')
        
        for j, arg in enumerate(zip(*args)):
            for i, (img, title) in enumerate(zip(arg, titles)):
                ax = plt.subplot(gs[i, j])
                ax.imshow(img)
                ax.set_axis_off()
                #ax[i][j].imshow(img)
                #ax[i][j].set_axis_off()
                if j == 2:
                    ax.set_title(title)
                    
        ax = plt.subplot(gs[4, :])
        ax.imshow(self.cleanObj.merged_character_image)
        ax.set_title('Final padded image ready for OCR')
        plt.show()
        
    def generate_plots(self):
        for plot_func in self.plot_funcs:
            plot_func()
        

                
path = '/IMAGE.jpg'

img = io.imread(path, as_grey = False)
imgray = io.imread(path, as_grey = True)

candidateObj = Find_candidates(imgray)
candidateObj.fetch()

characterObj = Get_characters(candidateObj)
characterObj.fetch()

cleanObj = Rotate_and_clean(characterObj)
cleanObj.fetch()
print cleanObj.character_string #The extracted license plate characters

Plot_stuff(cleanObj, path).generate_plots()


        
        
        
        